# Palliativ kemoterapi vid generaliserad sjukdom
tempDf <- df %>% 
  filter(
    !is.na(d0_year) & d0_year >= 2008 & 
      d0_diagnosgrp %in% "NSCLC" & 
      d0_stadium %in% "IV" & 
      a_who %in% 0:2 & 
      !is.na(a_kemo)
  ) %>% 
  mutate(
    group = d0_sjukhus,
    period = d0_year,
    outcome = a_kemo %in% 1
  ) %>% 
  select(
    group,
    period,
    outcome
  )