
Nationella lungcancerregistret (NLCR)
=====================================

Skript för clean data, beräknade variabler, kvalitetsindikatorer, etc.

Användning
----------

-   Ladda hem .RData-fil från INCA, t.ex. från någon av vyerna:
    -   *Lunga\_Nationell\_Vy*
    -   *Lunga\_EjID\_Nationell\_Infobearb*
    -   *Lunga\_Anm\_Reg\_Vy*
-   Kör **main.R** för clean data.
-   Se katalogen **indicators** för kvalitetsindikatorer.

Resultat
--------

En `tbl_df` med beräknade variabler, t.ex.:

    d0_ ...
    op0_ ...
